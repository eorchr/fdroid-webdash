# fdroid-webdash

web-app for browsing F-Droid repositories.

## web deployment hints

⚠️ Privacy Note: This app is not GDPR compliant, run at own risk. (Flutter SDK
may leaks user data to fonts.google.com and unpkg.com)

Release of this app are provided as [artifacts on
Gitlab-CI](https://gitlab.com/uniqx/fdroid-webdash/-/tags?sort=version_desc&search=v).

To deploy it, extract all files in `build/web` from our build artifacts to your
fdroid repository. It will try to guess the correct path of your index-v2.json
file based on browsers current location. There are 3 places relative to your
/fdroid/repo folder where you can deploy the web app:

* `/fdroid/repo/index.html` - will try to access `./index-v2.json`
* `/fdroid/index.html` - will try to access: `./repo/index-v2.json`
* `/index.html` - will try to access: `./fdroid/repo/index-v2.html`

(`index.html` is the entry point of this web application.)

## development hints


### do a web release build

```
flutter packages get
flutter build web --dart-define=FLUTTER_WEB_CANVASKIT_URL=/canvaskit/
```

The release artefact will show up in `build/web`.

### start a local dev server

Note: To get around CORS for accessing index files of remote F-Droid repos you might
want to use this handy tool before trying to run a dev server:
https://pub.dev/packages/flutter_cors

You can either open the project in Android Studio (with flutter plugin) or
start a development server in a terminal:

```
flutter run -d chrome --web-renderer html
```

(Using `-d web-server` will just start a dev webserver without launching chrome
browser, although in this case you'll have to take care of CORS some other
way.)
