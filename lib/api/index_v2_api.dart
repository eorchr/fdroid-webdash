import 'dart:convert';

import 'package:http/http.dart' as http;

import '../utils/text_helper.dart';

class IndexV2 {
  Repo repo;
  Packages packages;

  IndexV2({required this.repo, required this.packages});

  factory IndexV2.fromJson(Map<String, dynamic> json, String repoId) {
    var repo = Repo.fromJson(json['repo']);
    return IndexV2(
      repo: repo,
      packages: Packages.fromJson(json['packages'], repoId),
    );
  }

  static Future<IndexV2> fetch(String indexUrl) async {
    print('fetching: $indexUrl');
    final response = await http.get(Uri.parse(indexUrl));
    return IndexV2.fromJson(
      json.decode(utf8.decode(response.bodyBytes)),
      indexUrl.replaceAll("index-v2.json", ""),
    );
  }
}

class Repo {
  TranslatedString name;
  String address;
  String id;

  Repo({
    required this.name,
    required this.address,
    required this.id,
  });

  factory Repo.fromJson(Map<String, dynamic> json) => Repo(
        name: TranslatedString.fromJson(json['name']),
        address: json['address'],
        id: json['address'],
      );
}

class Packages {
  Map<String, Package> packages;

  Packages({required this.packages});

  factory Packages.fromJson(Map<String, dynamic> json, String repoId) {
    return Packages(
      packages: Map<String, Package>.from(json.map((k, v) =>
          MapEntry<String, Package>(k, Package.fromJson(v, k, repoId)))),
    );
  }
}

class Package {
  String packageName;
  Metadata metadata;
  Map<String, Version> versions;
  String repoUrl;

  Package({
    required this.packageName,
    required this.repoUrl,
    required this.metadata,
    required this.versions,
  });

  factory Package.fromJson(
          Map<String, dynamic> json, String packageName, String repoId) =>
      Package(
        packageName: packageName,
        repoUrl: repoId,
        metadata: Metadata.fromJson(json['metadata']),
        versions: json.containsKey('versions')
            ? Map<String, Version>.from(json['versions'].map(
                (k, v) => MapEntry<String, Version>(k, Version.fromJson(v))))
            : {},
      );
}

class Metadata {
  TranslatedString name;
  TranslatedString summary;
  TranslatedString description;
  TranslatedIcon? icon;
  TranslatedIcon? featureGraphic;
  Screenshots? screenshots;

  Metadata({
    required this.name,
    required this.summary,
    required this.description,
    required this.icon,
    required this.featureGraphic,
    required this.screenshots,
  });

  factory Metadata.fromJson(Map<String, dynamic> json) {
    return Metadata(
      name: TranslatedString.fromJson(json['name']),
      summary: TranslatedString.fromJson(json['summary']),
      description: TranslatedString.fromJson(json['description']),
      icon: TranslatedIcon.fromJson(json['icon']),
      featureGraphic: TranslatedIcon.fromJson(json['featureGraphic']),
      screenshots: json.containsKey('screenshots')
          ? Screenshots.fromJson(json['screenshots'])
          : null,
    );
  }
}

class Screenshots {
  TranslatedScreenshots? phone;

  Screenshots({required this.phone});

  factory Screenshots.fromJson(Map<String, dynamic> json) {
    return Screenshots(
        phone: json.containsKey('phone')
            ? TranslatedScreenshots.fromJson(json['phone'])
            : null);
  }
}

class TranslatedScreenshots {
  Map<String, List<FileInfo>> translated;

  TranslatedScreenshots({required this.translated});

  factory TranslatedScreenshots.fromJson(Map<String, dynamic> json) {
    return TranslatedScreenshots(
        translated: Map<String, List<FileInfo>>.from(json.map((k, v) =>
            MapEntry<String, List<FileInfo>>(
                k, List<FileInfo>.from(v.map((i) => FileInfo.fromJson(i)))))));
  }

  List<String> get iconUrls {
    var t = localize(translated);
    return t == null ? [] : List<String>.from(t.map((e) => e.name));
  }
}

class TranslatedIcon {
  Map<String, FileInfo> translated;

  TranslatedIcon({required this.translated});

  factory TranslatedIcon.fromJson(Map<String, dynamic>? json) {
    if (json == null) {
      return TranslatedIcon(translated: {});
    } else {
      return TranslatedIcon(
        translated: Map<String, FileInfo>.from(json.map(
            (k, v) => MapEntry<String, FileInfo>(k, FileInfo.fromJson(v)))),
      );
    }
  }

  String? get iconUrl {
    return localize<FileInfo>(translated)?.name;
  }
}

class FileInfo {
  String name;
  String sha256;
  int size;

  FileInfo({required this.name, required this.sha256, required this.size});

  factory FileInfo.fromJson(Map<String, dynamic> json) =>
      FileInfo(name: json['name'], sha256: json['sha256'], size: json['size']);
}

class TranslatedString {
  Map<String, String> translated;

  TranslatedString({required this.translated});

  factory TranslatedString.fromJson(Map<String, dynamic>? json) {
    if (json == null) {
      return TranslatedString(translated: {});
    } else {
      return TranslatedString(
        translated: Map<String, String>.from(json),
      );
    }
  }

  String? get() {
    return localize<String>(translated);
  }

  getOrDefault(String defaultValue) {
    return localize(translated) ?? defaultValue;
  }
}

class Version {
  DateTime added;
  TranslatedString whatsNew;
  Manifest manifest;
  FileInfo file;

  Version(
      {required this.whatsNew,
      required this.manifest,
      required this.added,
      required this.file});

  factory Version.fromJson(Map<String, dynamic> json) {
    return Version(
      whatsNew: TranslatedString.fromJson(json['whatsNew']),
      manifest: Manifest.fromJson(json['manifest']),
      added: DateTime.fromMicrosecondsSinceEpoch(json['added'], isUtc: true),
      file: FileInfo.fromJson(json['file']),
    );
  }
}

class Manifest {
  String versionName;

  Manifest({required this.versionName});

  factory Manifest.fromJson(Map<String, dynamic> json) {
    return Manifest(versionName: json['versionName']);
  }
}
