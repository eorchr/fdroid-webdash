import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../api/index_v2_api.dart';
import '../providers/packages_state.dart';
import '../utils/platform_specific.dart';

class LandingScreen extends StatelessWidget {
  const LandingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var packagesState = Provider.of<PackagesState>(context);
    Repo? repo = packagesState.repos.values.first;
    return CustomScrollView(
      slivers: [
        SliverAppBar(
          pinned: true,
          title: Text(repo.name.get() ?? 'Unnamed Repo'),
          backgroundColor: Theme.of(context).canvasColor,
        ),
        SliverToBoxAdapter(
          child: Column(
            children: [
              Text("Repo-Name: ${repo.name.get() ?? 'Unnamed Repo'}"),
              Text("Repo-Url: ${repo.address}"),
              InkWell(
                onTap: () async {
                  var url = Uri.parse(repo.address.toString());
                  platformSpecificUtils().openUrl(
                      fileUrl: url.replace(scheme: "fdroidrepo").toString());
                },
                child: const Text(
                  'Click here if you have F-Droid installed',
                  style: TextStyle(
                      decoration: TextDecoration.underline, color: Colors.blue),
                ),
              ),
              QrImageView(
                data: repo.address,
                version: QrVersions.auto,
                size: 200.0,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
