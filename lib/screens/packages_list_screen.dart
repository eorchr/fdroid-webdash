import 'dart:async';

import 'package:fdroidwebdash/widgets/responsive_padding.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../api/index_v2_api.dart';
import '../providers/packages_state.dart';
import '../widgets/app_icon.dart';
import '../widgets/loading_widget.dart';

class PackagesScreen extends StatefulWidget {
  const PackagesScreen({super.key});

  @override
  State<PackagesScreen> createState() => _PackagesScreenState();
}

class _PackagesScreenState extends State<PackagesScreen> {
  late String searchQuery;

  @override
  void initState() {
    super.initState();

    searchQuery = '';
  }

  @override
  Widget build(BuildContext context) {
    var packagesState = Provider.of<PackagesState>(context);

    return Stack(
      children: [
        AppList(
          packages: packagesState.search(searchQuery),
          startOffset: 80,
        ),
        SearchBar(performSearch: (query) {
          setState(() {
            searchQuery = query;
          },);
        }),
      ],
    );
  }
}

class AppList extends StatelessWidget {
  final double startOffset;
  final Map<String, Package> packages;

  const AppList({super.key, required this.packages, this.startOffset = 0});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.fromLTRB(0, startOffset, 0, 0),
      itemCount: packages.length,
      itemBuilder: (context, index) {
        return ResponsivePadding(
          child: PackageListItem(
            package: packages.values.elementAt(index),
            onTap: (package) {
              GoRouter.of(context).push('/packages/${package.packageName}');
            },
          ),
        );
      },
    );
  }
}

class PackageListItem extends StatelessWidget {
  // late String appName;
  final Package package;
  final FutureOr<void> Function(Package package)? onTap;

  const PackageListItem({super.key, required this.package, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (onTap != null) {
          onTap!(package);
        }
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
        child: Row(
          children: [
            AppIcon(package: package),
            const SizedBox(width: 8),
            Expanded(
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                      text: package.metadata.name.get() ?? 'Untitled',
                      //style: const TextStyle(fontWeight: FontWeight.bold, color: Colors.black)),
                      style: DefaultTextStyle.of(context)
                          .style
                          .apply(fontWeightDelta: 2)),
                  const TextSpan(text: ' '),
                  TextSpan(
                      text: package.metadata.summary.get() ?? '',
                      style: DefaultTextStyle.of(context).style),
                ]),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SearchBar extends StatefulWidget {
  final Function performSearch;

  const SearchBar({super.key, required this.performSearch});

  @override
  State<SearchBar> createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  var searchFieldController = TextEditingController();
  var searchFieldFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    searchFieldController.addListener(() {
      widget.performSearch(searchFieldController.text.length >= 2
          ? searchFieldController.text
          : '');
    });

    return Stack(
      children: [
        ResponsivePadding(
          child: Container(
            height: 12 + 32,
            color: Theme.of(context).scaffoldBackgroundColor,
          ),
        ),
        ResponsivePadding(
          child: Padding(
            padding: const EdgeInsets.all(12),
            child: Material(
              borderRadius: BorderRadius.circular(32),
              color: Theme.of(context).colorScheme.surface,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    width: 16,
                  ),
                  IconButton(
                    icon: const Icon(Icons.search),
                    onPressed: () => searchFieldFocusNode.requestFocus(),
                  ),
                  Expanded(
                    child: TextField(
                      controller: searchFieldController,
                      focusNode: searchFieldFocusNode,
                      decoration: const InputDecoration(
                          hintText: 'search apps',
                          border: OutlineInputBorder(
                            borderSide:
                                BorderSide(width: 0, style: BorderStyle.none),
                          )),
                    ),
                  ),
                  IconButton(
                    icon: const Icon(Icons.clear),
                    onPressed: () {
                      searchFieldController.clear();
                      searchFieldFocusNode.requestFocus();
                    },
                  ),
                  const SizedBox(
                    width: 16,
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
