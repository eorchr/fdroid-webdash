import 'package:fdroidwebdash/providers/packages_state.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Loading extends StatelessWidget {
  final Widget child;

  const Loading({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    final packagesState = Provider.of<PackagesState>(context);
    return packagesState.isInitializing()
        ? Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 160,
                    height: 160,
                    child: Image.asset('assets/fdroid-logo-240.png'),
                  ),
                  const SizedBox(height: 32),
                  const CircularProgressIndicator(),
                ],
              ),
            ),
          )
        : child;
  }
}
