import 'package:flutter/material.dart';

class ResponsivePadding extends StatelessWidget {
  final Widget child;

  const ResponsivePadding({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        double? fixedWidth;
        if (constraints.maxWidth >= 600) {
          fixedWidth = 600;
        }

        return fixedWidth == null
            ? child
            : Center(heightFactor: 1,child: SizedBox(width: fixedWidth, child: child));
      },
    );
  }
}
