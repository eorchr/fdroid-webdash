import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'flavor_state.dart';

class SettingsState with ChangeNotifier {
  List<String> repoUrls;

  SettingsState({required this.repoUrls});

  addRepoUrl(String repoUrl) {
    repoUrls.add(repoUrl.endsWith('/')
        ? repoUrl.substring(0, repoUrl.length - 1)
        : repoUrl);
    notifyListeners();
  }

  factory SettingsState.defaults(BuildContext context) {
    var flavor = Provider.of<FlavorSettings>(context, listen: false);

    // use fixed webserve
    if (flavor.isPwa() && kDebugMode) {
      return SettingsState(
          repoUrls: ["https://fdroid-webdash.h4x.at/fdroid/repo"]);
    }

    // default to local repo in web mode
    if (flavor.isPwa()) {
      String url = Uri.base.removeFragment().toString();
      url = url.substring(0, url.length - 1);
      if (!url.endsWith("/fdroid/repo")) {
        if (url.endsWith("/fdroid")) {
          url += "/repo";
        } else {
          url += "/fdroid/repo";
        }
      }
      return SettingsState(repoUrls: [url]);
    }

    // default repos in app mode
    return SettingsState(repoUrls: ['https://f-droid.org/fdroid/repo']);
  }
}
