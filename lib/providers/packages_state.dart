import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import '../api/index_v2_api.dart';
import '../providers/settings_state.dart';

class PackagesState with ChangeNotifier {
  Map<String, Package> packages;
  Map<String, Repo> repos;

  PackagesState({required this.packages, required this.repos});

  factory PackagesState.empty() => PackagesState(packages: {}, repos: {});

  void addPackages(Iterable<MapEntry<String, Package>> newPackages) {
    packages.addEntries(newPackages);
    notifyListeners();
  }

  static Future<PackagesState> fetch(BuildContext context) {
    var settings = Provider.of<SettingsState>(context, listen: false);
    Future<Iterable<IndexV2>> indexFetchers = Future.wait(
        settings.repoUrls.map((repoUrl) => Future<IndexV2>(() async {
              return await IndexV2.fetch('$repoUrl/index-v2.json');
            })));

    return Future(() async {
      Iterable<IndexV2> x = List.from(await indexFetchers);

      Map<String, Repo> repos = {};
      Map<String, Package> packages = {};

      for (var indexV2 in x) {
        repos[indexV2.repo.id] = indexV2.repo;
        indexV2.packages.packages.forEach((packageName, package) {
          if (!packages.keys.contains(packageName)) {
            packages[packageName] = package;
          }
        });
      }

      return PackagesState(packages: packages, repos: repos);
    });
  }

  Map<String, Package> search(String query) {
    if (query.length >= 2) {
      return Map.fromEntries(packages.entries.where((package) => package
          .value.metadata.name
          .getOrDefault("")
          .toLowerCase()
          .contains(query.toLowerCase())));
    } else {
      return packages;
    }
  }

  bool isInitializing() => packages.isEmpty;
}
