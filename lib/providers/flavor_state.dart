import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class FlavorSettings with ChangeNotifier {
  Flavor flavor;

  FlavorSettings({required this.flavor});

  setFlavor(Flavor flavor) {
    this.flavor = flavor;
    notifyListeners();
  }

  factory FlavorSettings.defaults(BuildContext context) {
    if (kIsWeb) {
      return FlavorSettings(flavor: Flavor.pwa);
    } else {
      return FlavorSettings(flavor: Flavor.mobile);
    }
  }

  bool isPwa() {
    return flavor == Flavor.pwa;
  }
}

enum Flavor {
  mobile(),
  pwa();
}