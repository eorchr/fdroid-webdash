import './platform_specific_stub.dart'
    if (dart.library.html) "./platform_specific_web.dart" as utils;

abstract class PlatformSpecificUtils {
  void downloadFile({required String fileUrl});
  void openUrl({required String fileUrl});
}

PlatformSpecificUtils platformSpecificUtils() {
  PlatformSpecificUtils instance = utils.getSingleton();
  return instance;
}
