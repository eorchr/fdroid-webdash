import './platform_specific.dart';

class PlatformSpecificUtilsStub extends PlatformSpecificUtils {
  @override
  void downloadFile({required String fileUrl}) {
    throw UnimplementedError(
        "downloadFile() not implemented for this platform");
  }

  @override
  void openUrl({required String fileUrl}) {
    throw UnimplementedError("openUrl() not implemented for this platform");
  }
}

PlatformSpecificUtils? instance;

PlatformSpecificUtils getSingleton() {
  instance ??= PlatformSpecificUtilsStub();
  return instance!;
}
