import 'dart:html' as html; // ignore: avoid_web_libraries_in_flutter

import './platform_specific.dart';

class PlatformSpecificUtilsWeb extends PlatformSpecificUtils {
  @override
  void downloadFile({required String fileUrl}) {
    var anchorElement = html.AnchorElement(href: fileUrl)..download = fileUrl;
    anchorElement.click();
  }

  @override
  void openUrl({required String fileUrl}) {
    var anchorElement = html.AnchorElement(href: fileUrl)..href = fileUrl;
    anchorElement.click();
  }
}

PlatformSpecificUtils? instance;

PlatformSpecificUtils getSingleton() {
  instance ??= PlatformSpecificUtilsWeb();
  return instance!;
}
